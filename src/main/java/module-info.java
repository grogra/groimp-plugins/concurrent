module util.concurrent {
	exports de.grogra.util.concurrent.producer;
	exports de.grogra.util.concurrent;

	requires rgg;
	requires raytracer;
	requires platform;
	requires platform.core;
	requires xl.impl;
	requires xl.core;
	requires graph;
	requires utilities;
}
