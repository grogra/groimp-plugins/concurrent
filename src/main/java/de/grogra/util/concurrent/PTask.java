package de.grogra.util.concurrent;

import java.io.Serializable;

import de.grogra.graph.GraphState;
import de.grogra.task.PartialTask;

/**
 * A PTask is the same a de.grogra.rgg.ConcurrentTask, but in the scope of a PTaskManager.
 */
public abstract class PTask implements PartialTask, Runnable,
	Serializable
{
	private transient GraphState state;
	transient boolean processed;
	
	public void markProcessed ()
	{
		processed = true;
	}
	
	public void setGraphState (GraphState state)
	{
		this.state = state;
	}
	
	public GraphState getGraphState ()
	{
		return state;
	}

}