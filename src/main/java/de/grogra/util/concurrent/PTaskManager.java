package de.grogra.util.concurrent;

import java.io.Serializable;
import java.util.TimerTask;

import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.Workbench;
import de.grogra.task.PartialTask;
import de.grogra.task.Task;
import de.grogra.util.ProgressMonitor;
import de.grogra.util.Utils;
import de.grogra.xl.util.ObjectList;

public class PTaskManager extends Task implements Serializable{

	private ObjectList<PTask> newTasks = new ObjectList<PTask> ();

	private ObjectList<PTask> tasks = new ObjectList<PTask> ();
	private int taskCount;

	private int solverCount = Runtime.getRuntime ().availableProcessors ();

	private TimerTask disposer;
	
	private ProgressMonitor monitor=null;
	private String progressText;
	private float progressValue = -2;
	private float progressResolution = 0.01f;
	private boolean doMonitor;


	public PTaskManager() {
		this(false);
	}
	
	public PTaskManager(boolean doMonitor) {
		super();
		this.doMonitor=doMonitor;
	}
	
	/**
	 * Invoked to remove the solvers after 5 seconds of inactivity.
	 * This in turn frees system resources (e.g., threads) needed by the
	 * solvers.
	 */
	private void rescheduleDisposer ()
	{
		if (disposer != null)
		{
			disposer.cancel ();
		}
		disposer = new TimerTask ()
		{
			@Override
			public void run ()
			{
				synchronized (PTaskManager.this)
				{
					if (!isSolving ())
					{
						removeSolvers ();
					}
				}
			}
		};
		Workbench.TIMER.schedule (disposer, 5000);
	}

	/**
	 * Sets the number of solvers (same virtual machine) to use.
	 * The default value is the number of processors which are available
	 * to the virtual machine.
	 * 
	 * @param count number of solvers to use
	 */
	public void setSolverCount (int count)
	{
		solverCount = count;
	}

	/**
	 * Adds a task to be solved during {@link #solve()}.
	 * 
	 * @param task single task to solve
	 */
	public void add (PTask task)
	{
		newTasks.add (task);
	}

	@Override
	protected PartialTask nextPartialTask (int solverIndex)
	{
		synchronized (tasks)
		{
			if (doMonitor && !tasks.isEmpty ()) {
				monitor.setProgress(String.valueOf( taskCount), -2);
			}
			return tasks.isEmpty () ? null : tasks.removeAt (0);
		}
	}

	@Override
	protected void dispose (PartialTask task)
	{
		if (((PTask) task).processed)
		{
			taskCount--;
		}
		else
		{
			synchronized (tasks)
			{
				tasks.add ((PTask) task);
			}
		}
	}

	@Override
	protected boolean done ()
	{
		return taskCount == 0;
	}

	@Override
	protected void prepareSolve ()
	{
		if (getSolverCount () == 0)
		{
			// no solvers there: create them
			for (int i = 0; i < solverCount; i++)
			{
				addSolver (new NonGraphLockedSolver (Workbench.current ()));
			}
		}
	}

	@Override
	public void solve ()
	{
		ObjectList<PTask> tmp = newTasks;
		newTasks = tasks;
		tasks = tmp;
		newTasks.clear ();
		taskCount = tasks.size ();
		if (doMonitor) {
			initProgressMonitor(UI.createProgressAdapter(Workbench.current()));
		}
		super.solve ();
	}

	@Override
	protected void finishSolve ()
	{
		rescheduleDisposer ();
		if (monitor!=null) {
			System.err.println("test");
			monitor.setProgress("done", ProgressMonitor.DONE_PROGRESS);
		}
	}

	void initProgressMonitor (ProgressMonitor monitor) {
		this.monitor=monitor;
	}


	public String describe() {
		return "Tasks to solve : " + newTasks.size () + "\n  Workers : " + solverCount;
	}
}
