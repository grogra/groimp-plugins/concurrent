package de.grogra.util.concurrent;

import de.grogra.graph.GraphState;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.Workbench;
import de.grogra.task.PartialTask;
import de.grogra.task.SolverInOwnThread;
import de.grogra.util.ThreadContext;

/**
 * A solver for PTask that should not make modification on the graph.
 * The Solver do not processes grahp queues, so it should not be albe 
 * to push modification in the graph after the execution.
 * 
 * NonGraphLockedSolver do not derive the graph before starting a solve. 
 */
public class NonGraphLockedSolver extends SolverInOwnThread{
	private final Workbench workbench;
	private GraphState state;

	private final Object initLock = new Object ();

	public NonGraphLockedSolver (Workbench wb)
	{
		workbench = wb;
	}

	@Override
	protected void solveImpl (PartialTask task)
	{
		PTask t = (PTask) task;
		t.setGraphState (state);
		t.run ();
		t.markProcessed ();
	}

	@Override
	protected Thread createThread ()
	{
		Thread t = new Thread (this, toString ());
		t.setPriority (Thread.MIN_PRIORITY);
		return t;
	}

	@Override
	public void run ()
	{
		synchronized (initLock)
		{
			Registry.setCurrent (workbench);
			Workbench.setCurrent (workbench);
			ThreadContext c = ThreadContext.current ();
			ThreadContext wc = workbench.getJobManager ().getThreadContext (); 
			c.setPriority (ThreadContext.MAX_PRIORITY);
			state = GraphState.get (workbench.getRegistry ().getProjectGraph (), wc).forContext (c);
		}
		try
		{
			super.run ();
		}
		finally
		{
			Registry.setCurrent (null);
			Workbench.setCurrent (null);
			state.dispose ();
			state = null;
		}
	}

}
